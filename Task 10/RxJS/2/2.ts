import {Observable} from "rxjs"

const stream$ = new Observable(observer => {
    for (let i = 5; i > 0; i -= 1) {
        observer.next(i)
    }
    observer.error('Some error')
})

stream$.subscribe(
    value => console.log(value),
    error => console.log(error)
)