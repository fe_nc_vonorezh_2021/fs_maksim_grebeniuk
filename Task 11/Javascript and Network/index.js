const cityInput = document.querySelector('#city-input')
const url = 'https://api.openweathermap.org/data/2.5/weather'
const appid = '87a4e3b57fd5877a435275e9fae66ccd'
const responseContent = document.querySelector('.responseContent')

const showWeather = async () => {
    const response = await fetch(`${url}?q=${cityInput.value}&appid=${appid}`)
        .then(resp => resp.json())

    responseContent.innerHTML = response.cod != '200' ?
        `<h1>Error: ${response.message}</h1>` :
        `
            <img src="https://openweathermap.org/img/wn/${response.weather[0].icon}@2x.png" />
            <div>
                <p>${response.weather[0].main}</p>
                <p>${Math.round(response.main.temp - 273)}&deg;</p>
                <p>${new Date()}</p>
            </div>
        `
}