import { MovieProps } from "../types/MovieProps"

// export interface MovieProps {
//     title: string
//     director: string
//     genre: string
//     yearOfPublishing: number
//     ageRestrictions: number
// }

export class Movie {
    private _title: string
    protected _director: string
    protected _genre: string
    protected _yearOfPublishing: number
    private _ageRestrictions: number

    constructor({title, director, genre, yearOfPublishing, ageRestrictions}: MovieProps) {
        this._title = title
        this._director = director
        this._genre = genre
        this._yearOfPublishing = yearOfPublishing
        this._ageRestrictions = ageRestrictions
    }
    set title(value: string) {
        this._title = value
    }
    get title(): string {
        return this._title
    }
    set ageRestrictions(value: number) {
        this._ageRestrictions = value
    }
    get ageRestrictions(): number {
        return this._ageRestrictions
    }
    get info(): string {
        return `The title of the film is "${this.title}". The director is ${this._director}. The genre is ${this._genre}. Year of publication - ${this._yearOfPublishing}. Age restrictions - +${this.ageRestrictions}`
    }
}