class Cinema {
    constructor(name, movies = [], viewers = []) {
        this.name = name
        this.movies = movies
        this.viewers = viewers
    }
    get info() {
        return `In the сinema "${this.name}" there are movies: ${this.movies.length ? this.movies.map(movie => movie.title).join(', ') : '-'}. Viewers: ${this.viewers.length ? this.viewers.map(viewer => viewer.name).join(', ') : '-'}`
    }
    set name(value) {
        this._name = value
    }
    get name() {
        return this._name
    }
    set movies(value) {
        this._movies = value
    }
    get movies() {
        return this._movies
    }
    set viewers(value) {
        this._viewers = value
    }
    get viewers() {
        return this._viewers
    }
    addNewMovie(movie) {
        this.movies.push(movie)
    }
    getAvailableMovies(viewer) {
        return this.movies.filter(movie => movie.ageRestrictions <= viewer.age).map(movie => movie.title)
    }
}

class Movie {
    constructor(title, director, genre, yearOfPublishing, ageRestrictions = 6) {
        this.title = title
        this.director = director
        this.genre = genre
        this.yearOfPublishing = yearOfPublishing
        this.ageRestrictions = ageRestrictions
    }
    set title(value) {
        this._title = value
    }
    get title() {
        return this._title
    }
    set ageRestrictions(value) {
        this._ageRestrictions = value
    }
    get ageRestrictions() {
        return this._ageRestrictions
    }
    get info() {
        return `The title of the film is "${this.title}". The director is ${this.director}. The genre is ${this.genre}. Year of publication - ${this.yearOfPublishing}. Age restrictions - +${this.ageRestrictions}`
    }
}

class HorrorMovie extends Movie {
    constructor(title, director, yearOfPublishing, numberOfKills) {
        super(title, director, 'horror', yearOfPublishing, 18)
        this.numberOfKills = numberOfKills
    }
    get info() {
        return `The title of the film is "${this.title}". The director is ${this.director}. The genre is ${this.genre}. Year of publication - ${this.yearOfPublishing}. Age restrictions - +${this.ageRestrictions}. Number of kills ${numberOfKills}`
    }
}

class Cartoon extends Movie {
    constructor(title, director, yearOfPublishing) {
        super(title, director, 'cartoon', yearOfPublishing, 0)
    }
}

class Viewer {
    constructor(name, age) {
        this.name = name
        this.age = age
    }
    get info() {
        return `Viewer ${this.name}. Age ${this.age}`
    }
    set name(value) {
        this._name = value
    }
    get name() {
        return this._name
    }
    set age(value) {
        this._age = value
    }
    get age() {
        return this._age
    }
}

const cinema = new Cinema('Star')
console.log(cinema.info)

const soulCartoon = new Cartoon('Soul', 'Pete Docter', 2020)
console.log(soulCartoon.info)

const duneMovie = new Movie('Dune', 'Denis Villeneuve', 'fantastic', 2021)
console.log(duneMovie.info)
duneMovie.ageRestrictions = 12
console.log(duneMovie.info)

const it = new HorrorMovie('It', 'Andres Muschietti', 2017, 6)

cinema.movies = [soulCartoon, duneMovie]
cinema.addNewMovie(it)

const gregory = new Viewer('Gregory', 1)
console.log(gregory.info)
const boris = new Viewer('Boris', 21)
console.log(boris.info)
const vyacheslav = new Viewer('Vyacheslav', 14)
console.log(vyacheslav.info)

cinema.viewers = [gregory, boris, vyacheslav]
console.log('Available movies for Gregory:', cinema.getAvailableMovies(gregory).join(', '))
console.log('Available movies for Boris:', cinema.getAvailableMovies(boris).join(', '))
console.log('Available movies for Vyacheslav:', cinema.getAvailableMovies(vyacheslav).join(', '))

console.log(cinema.info)