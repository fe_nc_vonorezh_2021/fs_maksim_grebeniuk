import { log } from '../decorators/decorator'

export class CinemaFactory {
    @log
    create<T>(type: { new(name: string): T }, name: string): T {
        return new type(name)
    }
}