const createArray = (length, maxEl) => Array.from({ length }, () => Math.floor(Math.random() * maxEl))

const array = createArray(20, 100)
console.log(`source array: ${array}`)

const arraySort = (arr, dir = 'asc') => {
    if (arr.length === 0) return [];
    const a = [], b = [], p = arr[0];
    for (let i = 1; i < arr.length; i++) {
        if (dir === 'asc') {
            if (arr[i] < p) a[a.length] = arr[i];
            else b[b.length] = arr[i];
        } else {
            if (arr[i] > p) a[a.length] = arr[i];
            else b[b.length] = arr[i];
        }
    }
    return arraySort(a, dir).concat(p, arraySort(b, dir));
}

console.log(`sorted array asc: ${arraySort(array)}`)
console.log(`sorted array desc: ${arraySort(array, 'desc')}`)

const sumOfSquares = (arr) => arr.map(item => item % 2 !== 0 ? item : null).filter(item => item !== null).reduce((prevVal, curVal) => prevVal += curVal * curVal)
console.log(`sum of squares of odd elements: ${sumOfSquares(array)}`)