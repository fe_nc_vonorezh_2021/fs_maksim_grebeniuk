const table = document.getElementsByTagName('table')[0]
const defaultTr = '<tr><td onclick="editTr(this)"></td><td onclick="editTr(this)"></td><td onclick="editTr(this)"></td><td onclick="editTr(this)"></td><td class="td-button"><button onclick="deleteTr(this)">&#10006;</button></td></tr>'

const dialog = document.querySelector('.back-dialog')
const dialogText = document.querySelector('.dialog-text')
const dialogInput = document.getElementById('dialog-input')
const dialogSave = document.getElementById('dialog-save')

const deleteTr = el => el.parentNode.parentNode.remove()
const addTr = () => table.innerHTML += defaultTr

const editTr = el => {
    if (el.textContent) {
        dialogText.innerHTML = 'Edit the cell'
        dialogInput.value = el.textContent
    } else {
        dialogText.innerHTML = 'Add value to cell'
        dialogInput.value = ''
    }
    dialog.style.display = 'block'
    dialogSave.addEventListener('click', () => {
        if (el) {
            el.textContent = dialogInput.value
            el = null
            dialog.style.display = 'none'
        }
    })
}