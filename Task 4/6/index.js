const startGame = () => {
    const number = Math.floor(Math.random() * 1001)
    let count = 0
    let enteredNumber = prompt('Введите число')
    while(number !== enteredNumber) {
        count++
        if(!enteredNumber || isNaN(enteredNumber)) enteredNumber = prompt('Введите корректное число')
        else if(number < enteredNumber) enteredNumber = prompt('Искомое число меньше')
        else if(number > enteredNumber) enteredNumber = prompt('Искомое число больше')
        else {
            const response = confirm(`Вы угадали! Количество попыток: ${count}. Начать заново?`)
            if(response) startGame()
            else return
        }
    }   
}