export interface MovieProps {
    title: string
    director: string
    genre: string
    yearOfPublishing: number
    ageRestrictions: number
}