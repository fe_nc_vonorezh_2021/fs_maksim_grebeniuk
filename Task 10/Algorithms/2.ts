function partition(list: number[], from: number, to: number): number {
    let i: number = from
    let pivot: number = list[to]

    for (let j = from; j < to; ++j) {
        if (list[j] < pivot) {
            [list[j], list[i]] = [list[i], list[j]]
            ++i
        }
    }

    [list[i], list[to]] = [list[to], list[i]]
    return i
}

function quickSortRange(list: number[], from: number, to: number) {
    if (from < to) {
        const p = partition(list, from, to)
        quickSortRange(list, from, p - 1)
        quickSortRange(list, p + 1, to)
    }
}

function quickSort(list: number[]) {
    quickSortRange(list, 0, list.length - 1)
}

const createArray = (length: number, maxEl: number) => Array.from({ length }, () => Math.floor(Math.random() * maxEl))

const array = createArray(10, 100)
console.log(arr)
quickSort(array)
console.log(array)