import { Cinema } from "./models/Cinema"
import { HorrorMovie, HorrorProps } from "./models/HorrorMovie"
import { Movie } from "./models/Movie"
import { MovieProps } from "./types/MovieProps"
import { MovieFactory } from "./models/MovieFactory"
import { CinemaFactory } from "./models/CinemaFactory"
import { Viewer } from "./models/Viewer"

const cinemaFactory = new CinemaFactory()

const cinema = cinemaFactory.create<Cinema>(Cinema, 'Star')
console.log(cinema.info)

const movieFactory = new MovieFactory()

const soulCartoon = movieFactory.create<Movie, MovieProps>(Movie, { title: 'Soul', director: 'Pete Docter', genre: 'cartoon', yearOfPublishing: 2020, ageRestrictions: 0 })
console.log(soulCartoon.info)

const duneMovie = movieFactory.create<Movie, MovieProps>(Movie, { title: 'Dune', director: 'Denis Villeneuve', genre: 'fantastic', yearOfPublishing: 2021, ageRestrictions: 0 })
console.log(duneMovie.info)
duneMovie.ageRestrictions = 12
console.log(duneMovie.info)

const it = movieFactory.create<HorrorMovie, HorrorProps>(HorrorMovie, { title: 'It', director: 'Andres Muschietti', genre: 'horror', yearOfPublishing: 2017, ageRestrictions: 19, numberOfKills: 6 })

cinema.movies = [soulCartoon, duneMovie]
cinema.addNewMovie(it)

const gregory = new Viewer('Gregory', 1)
console.log(gregory.info)
const boris = new Viewer('Boris', 21)
console.log(boris.info)
const vyacheslav = new Viewer('Vyacheslav', 14)
console.log(vyacheslav.info)

cinema.viewers = [gregory, boris, vyacheslav]
console.log('Available movies for Gregory:', cinema.getAvailableMovies(gregory).join(', '))
console.log('Available movies for Boris:', cinema.getAvailableMovies(boris).join(', '))
console.log('Available movies for Vyacheslav:', cinema.getAvailableMovies(vyacheslav).join(', '))

console.log(cinema.info)