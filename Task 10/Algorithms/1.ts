class Node<T> {
    data: T
    next: Node<T> | null
    previous: Node<T> | null

    constructor(data: T) {
        this.data = data
        this.next = this.previous = null
    }
}

export class DoublyLinkedList<T> {
    head: Node<T> | null
    tail: Node<T> | null
    length: number

    constructor(data: T | null = null) {
        if (data) {
            const headNode = new Node(data)
            this.head = headNode
            this.tail = headNode
            this.length = 1
        } else {
            this.head = null
            this.tail = null
            this.length = 0
        }
    }

    private newNodeAssignment(currentNode: Node<T>, data: T) : Node<T>{
        let newNode = new Node(data)
        currentNode.previous.next = newNode
        newNode.previous = currentNode.previous
        newNode.next = currentNode
        currentNode.previous = newNode
        this.length++
        return newNode.next
    }

    private removeNode(node: Node<T>): T {
        const data = node.data
        node.previous.next = node.next
        node.next.previous = node.previous
        this.length--

        node.data = null
        node.next = node.previous = null

        return data
    }

    isEmpty(): boolean {
        return this.length === 0
    }

    get(index: number): Node<T> {
        if (index < 0 || index > this.length) throw "Illegal argument"
        if(index > this.length / 2) {
            let i = (this.length - 1) - index
            let tmp = this.tail
            while(i > 0) {
                tmp = tmp.previous
                i--
            }
            return tmp
        } else {
            let tmp = this.head
            for(let i = 0; i < index; i++) {
                tmp = tmp.next
            }
            return tmp
        }
    }

    add(data: T) {
        let newNode = new Node(data)
        if (this.isEmpty()) {
            this.head = this.tail = newNode
        } else {
            this.tail.next = newNode
            newNode.previous = this.tail
            this.tail = newNode
        }
        this.length++
    }

    addFirst(data: T) {
        if (this.isEmpty()) {
            this.head = this.tail = new Node(data)
        } else {
            let node = new Node(data)
            this.head.previous = node
            node.next = this.head
            this.head = node
        }
        this.length++
    }

    addAt(index: number, data: T) {
        if (index < 0 || index > this.length) throw "Illegal argument"
        if (index === 0) this.addFirst(data)
        else if (index === this.length) this.add(data)
        else {
            if (index < this.length / 2) {
                let currentNode = this.head
                for (let i = 0; i < index; i++) {
                    currentNode = currentNode.next
                }
                this.newNodeAssignment(currentNode, data)
            } else {
                let currentNode = this.tail
                for (let i = this.length - 1; i > index; i--) {
                    currentNode = currentNode.previous
                }
                this.newNodeAssignment(currentNode, data)
            }
        }
    }

    deleteFirst(): T {
        if (this.isEmpty()) throw "The LinkedList is empty"

        let data: T = this.head.data
        this.head = this.head.next
        this.length--

        if (this.isEmpty()) this.tail = null
        else this.head.previous = null

        return data
    }

    deleteLast(): T {
        if (this.isEmpty()) throw "The LinkedList is empty"

        let data: T = this.head.data
        this.tail = this.tail.previous
        this.length--

        if (this.isEmpty()) this.head = null
        else this.tail.next = null

        return data
    }

    deleteNode(node: Node<T>){
        if(!node.previous) this.deleteFirst()
        else if(!node.next) this.deleteLast()
        else{
            this.removeNode(node)
        }
    }

    deleteAt(index: number): T {
        if (index < 0 || index >= this.length) throw "Illegal argument"
        if (index === 0) return this.deleteFirst()
        else if (index === this.length - 1) return this.deleteLast()
        else {
            if (index < this.length / 2) {
                let currentNode = this.head
                for (let i = 0; i < index; i++) {
                    currentNode = currentNode.next
                }
                return this.removeNode(currentNode)
            } else {
                let currentNode = this.tail
                for (let i = this.length - 1; i > index; i--) {
                    currentNode = currentNode.previous
                }
                return this.removeNode(currentNode)
            }
        }
    }

    deleteWith(data: T) {
        let node = this.head
        while(node) {
            if(node.data === data){
                this.deleteNode(node)
                return
            }
            node = node.next
        }
    }

    editAt(index: number, data: T) {
        if (index < 0 || index >= this.length) throw "Illegal argument"
        this.get(index).data = data
    }
}

const doublyLinkedList: DoublyLinkedList<number> = new DoublyLinkedList()
console.log(doublyLinkedList)
doublyLinkedList.add(1)
doublyLinkedList.add(2)
doublyLinkedList.add(3)
doublyLinkedList.add(4)
doublyLinkedList.addAt(2, 5)
console.log(doublyLinkedList)
doublyLinkedList.editAt(2, 8)
console.log(doublyLinkedList.get(2))
doublyLinkedList.deleteAt(2)
doublyLinkedList.deleteLast()
console.log(doublyLinkedList)