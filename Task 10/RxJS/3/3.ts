import { fromEvent, merge } from "rxjs"

const body = document.querySelector('body')
const getRandomColor = () => {
    return '#' + (Math.random().toString(16) + '000000').substring(2,8).toUpperCase()
}

const firstStream$ = fromEvent(document.querySelector('.first-button'), 'click')
const secondStream$ = fromEvent(document.querySelector('.second-button'), 'click')
const thirdStream$ = fromEvent(document.querySelector('.third-button'), 'click')

merge(firstStream$, secondStream$, thirdStream$).subscribe(value => body.style.background = getRandomColor())