import { Movie } from "./Movie"
import { MovieProps } from "../types/MovieProps"

export interface HorrorProps extends MovieProps {
    numberOfKills: number
}

export class HorrorMovie extends Movie {
    private readonly _numberOfKills: number

    constructor(movie: HorrorProps) {
        super(movie)
        this._numberOfKills = movie.numberOfKills
    }
    get info(): string {
        return `The title of the film is "${this.title}". The director is ${this._director}. The genre is ${this._genre}. Year of publication - ${this._yearOfPublishing}. Age restrictions - +${this.ageRestrictions}. Number of kills ${this._numberOfKills}`
    }
}