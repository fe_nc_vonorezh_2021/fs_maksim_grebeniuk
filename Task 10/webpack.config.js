const path = require('path')

module.exports = {
  entry: path.join(__dirname, './Task 10/RxJS/3/3.ts'),
  devtool: 'inline-source-map',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  output: {
    filename: '3.js',
    path: path.resolve(__dirname, 'Task 10/RxJS/3'),
  },
};