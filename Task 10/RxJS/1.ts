import {filter, from} from 'rxjs'

const isSimple = (n: number) => {
    if (n === 1 || n === 0) return false
    else {
        for (let i = 2; i < n; i++) {
            if(n % i === 0) return false
        }
        return true
    }
}

const arrayForStream: number[] = Array.from(Array(101).keys())
const stream$ = from(arrayForStream)
    .pipe(
        filter(value => isSimple(value))
    )

stream$.subscribe(value => console.log(value))
