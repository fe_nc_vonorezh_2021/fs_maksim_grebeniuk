import { Movie } from "./Movie"
import { Viewer } from "./Viewer"

export class Cinema {
    private _name: string
    private _movies: Array<Movie>
    private _viewers: Array<Viewer>

    constructor(name: string) {
        this._name = name
        this._movies = []
        this._viewers = []
    }
    get info(): string {
        return `In the сinema "${this.name}" there are movies: ${this.movies.length ? this.movies.map(movie => movie.title).join(', ') : '-'}. Viewers: ${this.viewers.length ? this.viewers.map(viewer => viewer.name).join(', ') : '-'}`
    }
    set name(value: string) {
        this._name = value
    }
    get name(): string {
        return this._name
    }
    set movies(value: Array<Movie>) {
        this._movies = value
    }
    get movies(): Array<Movie> {
        return this._movies
    }
    set viewers(value: Array<Viewer>) {
        this._viewers = value
    }
    get viewers(): Array<Viewer> {
        return this._viewers
    }
    addNewMovie(movie: Movie): void {
        this.movies.push(movie)
    }
    getAvailableMovies(viewer: Viewer): Array<string> {
        return this.movies.filter(movie => movie.ageRestrictions <= viewer.age).map(movie => movie.title)
    }
}