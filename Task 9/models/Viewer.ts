export class Viewer {
    private _name: string
    private _age: number

    constructor(name: string, age: number) {
        this._name = name
        this._age = age
    }
    get info(): string {
        return `Viewer ${this.name}. Age ${this.age}`
    }
    set name(value: string) {
        this._name = value
    }
    get name(): string {
        return this._name
    }
    set age(value: number) {
        this._age = value
    }
    get age(): number {
        return this._age
    }
}