let a = 0
let b = 1

// first way
console.log(`original numbers: a = ${a}, b = ${b}`)
const c = a
a = b
b = c
console.log(`converted numbers: a = ${a}, b = ${b}`)

// second way
console.log(`original numbers: a = ${a}, b = ${b}`)
[b, a] = [a, b]
console.log(`converted numbers: a = ${a}, b = ${b}`)