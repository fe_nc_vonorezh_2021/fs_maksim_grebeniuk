const name = document.querySelector('.name')
const surname = document.querySelector('.surname')
const email = document.querySelector('.email')
const phone = document.querySelector('.phone')
const message = document.querySelector('.message')
const sendButton = document.querySelector('.send-button')
const dialog = document.getElementById('dialog')
const dialogText = document.querySelector('.dialog-text')
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const phoneRegex = /\+\d\(\d{3}\)\d{2}-\d{2}-\d{3}/

document.querySelectorAll('input').forEach(input => input.addEventListener('input', event => {
    checkSendButton()
    const form = JSON.parse(localStorage.getItem('form')).form
    form[event.target.classList[0]] = event.target.value
    localStorage.setItem('form', JSON.stringify({ form }))
}))

const initForm = () => {
    try {
        const form = JSON.parse(localStorage.getItem('form')).form
        if (form) {
            name.value = form.name
            surname.value = form.surname
            email.value = form.email
            phone.value = form.phone
            message.value = form.message
        }
        checkSendButton()
    } catch (e) { 
        const form = {
            name: '',
            surname: '',
            email: '',
            phone: '',
            message: '',
        }
        localStorage.setItem('form', JSON.stringify({ form }))
    }
}

const hideInvalidFields = () => {
    [name, surname, email, phone, message].forEach(field => {
        field.classList.remove('invalid-field')
        document.querySelectorAll('.invalid-message').forEach(el => el.innerHTML = '')
    })
}

const showInvalidFileds = fields => {
    hideInvalidFields()
    fields.forEach(field => {
        field.classList.add('invalid-field')
        field.parentElement.querySelector('.invalid-message').innerHTML = 'invalid field'
        field.value = ''
    })
}

const sendForm = () => {
    const form = {}
    const invalidFields = []

    if (name.value) form.name = name.value 
    else invalidFields.push(name)

    if (surname.value) form.surname = surname.value 
    else invalidFields.push(surname)

    if (email.value && emailRegex.test(email.value)) form.email = email.value
    else invalidFields.push(email)

    if (phone.value) {
        if (phoneRegex.test(phone.value)) form.phone = phone.value 
        else invalidFields.push(phone)
    } else form.phone = ''

    if (message.value) form.message = message.value 
    else invalidFields.push(message)
    
    if (form.name && form.surname && form.email && (form.phone || form.phone === '') && form.message) {
        hideInvalidFields()
        if (document.cookie !== 'requestSent=true') openDialog(`${form.name} ${form.surname}, спасибо за обращение!`)
        else openDialog(`${form.name} ${form.surname}, ваше обращение обрабатывается!`)
        document.cookie = 'requestSent=true'
    }
    else showInvalidFileds(invalidFields)
}

const checkSendButton = () => {
    if (name.value && surname.value && email.value && message.value) {
        sendButton.disabled = false
        sendButton.textContent = 'send'
    } else {
        sendButton.disabled = true
        sendButton.textContent = 'fill in the fields'
    }
}

const closeDialog = () => {
    dialog.style.display = 'none'
}

const openDialog = dialogMessage => {
    dialogText.innerHTML = dialogMessage
    dialog.style.display = 'block'
}

initForm()