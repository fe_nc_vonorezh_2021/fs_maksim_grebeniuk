import { log } from '../decorators/decorator'

export class MovieFactory {
    @log
    create<T, S>(type: { new(props: S): T; }, props: S): T {
        return new type(props)
    }
}