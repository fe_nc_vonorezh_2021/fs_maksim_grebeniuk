const input = document.getElementById('input')
const history = document.querySelector('.history-container')
let equalWasPressed = false
document.querySelectorAll('.reg-button').forEach(button => button.addEventListener('click', event => {
    if (equalWasPressed) {
        input.value = ''
        equalWasPressed = false
    }
    input.value.length < 10 ? input.value += event.target.textContent : input.value
}))
function clean() {
    input.value = ""
}
const insert = num => {
    if (/\d/.test(input.value.slice(-1))) {
        input.value.length < 10 ? input.value = input.value + num : input.value
    }
}
const addToHistory = message => {
    const p = document.createElement('p')
    p.innerHTML = message
    history.append(p)
}
const clearHistory = () => history.innerHTML = ''
const back = () => input.value = input.value.slice(0, -1);
const equal = () => {
    if (input.value && /\d/.test(input.value.slice(-1))) {
        equalWasPressed = true
        addToHistory(`${input.value}=${eval(input.value).toString().substring(0, 10)}`)
        input.value = eval(input.value).toString().substring(0, 10)
    }
}
const pow = () => {
    if (input.value && /\d/.test(input.value.slice(-1))) {
        addToHistory(`${input.value}&sup2;=${Math.pow(eval(input.value), 2).toString().substring(0, 10)}`)
        input.value = Math.pow(eval(input.value), 2).toString().substring(0, 10)
    }
}
const sqrt = () => {
    if (input.value && /\d/.test(input.value.slice(-1))) {
        addToHistory(`&radic;${input.value}=${Math.sqrt(eval(input.value)).toString().substring(0, 10)}`)
        input.value = Math.sqrt(eval(input.value)).toString().substring(0, 10)
    }
}
const point = () => {
    if (/\d/.test(input.value.slice(-1))) {
        const exp = input.value.split(/[-+\/*]/)
        if (exp[exp.length - 1].indexOf('.') > -1) return
        input.value.length < 10 ? input.value = input.value + '.' : input.value
    }
}