const gulp = require('gulp')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')

function js() {
    return gulp.src('./Task 4/**/**.js')
        .pipe(concat('index.mini.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./Task 8/js/'))
}

exports.js = js